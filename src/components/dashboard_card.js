import React from 'react';
import { Card, Typography } from '@mui/material';
import Button from '@mui/material/Button';

const DashboardCard = () => (
  <>
    <Card
      variant="outlined"
      container
      justifyContent="center"
      alignItems="center"
      elevation={0}
      sx={{
        alignItems: 'center',
        backgroundColor: '#b3daff',
        borderRadius: 1,
        textAlign: 'center',
        p: 2,
        m: 1
      }}
    >

      <Button variant="text" sx={{ color: '#333' }} href="/dashboard">
        <Typography
          color="textSecondary"
          variant="overline"
          alignItems="center"
        >
          Home
        </Typography>
      </Button>
    </Card>

    <Card
      variant="outlined"
      elevation={0}
      sx={{
        alignItems: 'center',
        backgroundColor: '#b3daff',
        borderRadius: 1,
        textAlign: 'center',
        p: 2,
        m: 1,
      }}
    >
      <Button variant="text" sx={{ color: '#333' }} href="/active-users">
        <Typography
          color="textSecondary"
          variant="overline"
        >
          Active Users
        </Typography>
      </Button>
    </Card>
    <Card
      variant="outlined"
      elevation={0}
      sx={{
        alignItems: 'center',
        backgroundColor: '#b3daff',
        borderRadius: 1,
        textAlign: 'center',
        p: 2,
        m: 1
      }}
    >
      <Button variant="text" sx={{ color: '#333' }} href="/care-team">
        <Typography
          color="textSecondary"
          variant="overline"
          justifyContent="center"
          alignItems="center"
        >
          Care Team
        </Typography>
      </Button>
    </Card>
    <Card
      variant="outlined"
      elevation={0}
      sx={{
        alignItems: 'center',
        backgroundColor: '#b3daff',
        borderRadius: 1,
        textAlign: 'center',
        p: 2,
        m: 1
      }}
    >
      <Button variant="text" sx={{ color: '#333' }} href="/alert">
        <Typography
          color="textSecondary"
          variant="overline"
          alignItems="center"
        >
          Report
        </Typography>
      </Button>
    </Card>

    <Card
      variant="outlined"
      elevation={0}
      sx={{
        alignItems: 'center',
        backgroundColor: '#b3daff',
        borderRadius: 1,
        textAlign: 'center',
        p: 2,
        m: 1,
      }}
    >
      <Button variant="text" sx={{ color: '#333' }} href="/patient-communication">
        <Typography
          color="textSecondary"
          variant="overline"
          whiteSpace="normal"
          noWrap="false"
        >
          Patient
          Communication
        </Typography>
      </Button>
    </Card>
    <Card
      variant="outlined"
      elevation={0}
      sx={{
        alignItems: 'center',
        backgroundColor: '#b3daff',
        borderRadius: 1,
        textAlign: 'center',
        p: 2,
        m: 1
      }}
    >
      <Button variant="text" sx={{ color: '#333' }} href="/practice-setup">
        <Typography
          color="textSecondary"
          variant="overline"
          justifyContent="center"
          alignItems="center"
        >
          Admin
        </Typography>
      </Button>
    </Card>
    <Card
      variant="outlined"
      elevation={0}
      sx={{
        alignItems: 'center',
        backgroundColor: '#b3daff',
        borderRadius: 1,
        textAlign: 'center',
        p: 2,
        m: 1
      }}
    >
      <Button variant="text" sx={{ color: '#333' }} href="/device-management">
        <Typography
          color="textSecondary"
          variant="overline"
          justifyContent="center"
          alignItems="center"
        >
          Device Manager
        </Typography>
      </Button>
    </Card>
  </>
);

export default DashboardCard;
