import { Link as RouterLink, matchPath, useLocation } from 'react-router-dom';
import { Drawer, List, ListItem, ListItemIcon } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import NotificationAddIcon from '@mui/icons-material/NotificationAdd';
import PersonIcon from '@mui/icons-material/Person';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import SettingsIcon from '@mui/icons-material/Settings';
import KeyboardVoiceIcon from '@mui/icons-material/KeyboardVoice';
import ViewListIcon from '@mui/icons-material/ViewList';
import { Home as HomeIcon } from '../icons/home';

const items = [
  {
    href: '/',
    icon: SearchIcon,
    label: 'Home'
  },
  {
    href: '/',
    icon: HomeIcon,
    label: 'Home'
  },
  {
    href: '/',
    icon: NotificationAddIcon,
    label: 'Home'
  },
  {
    href: '/',
    icon: PersonIcon,
    label: 'Home'
  },
  {
    href: '/',
    icon: CalendarMonthIcon,
    label: 'Home'
  },
  {
    href: '/',
    icon: SettingsIcon,
    label: 'Home'
  },
  {
    href: '/',
    icon: KeyboardVoiceIcon,
    label: 'Home'
  },
  {
    href: '/',
    icon: PowerSettingsNewIcon,
    label: 'Home'
  },
  {
    href: '/',
    icon: ViewListIcon,
    label: 'Home'
  },
];

export const Sidebar = () => {
  const location = useLocation();

  return (
    <Drawer
      open
      sx={{ zIndex: 1000 }}
      variant="permanent"
      PaperProps={{
        sx: {
          backgroundColor: '#666666',
          display: 'flex',
          flexDirection: 'column',
          height: 'calc(100% - 64px)',
          p: 1,
          top: 70,
          width: 75
        }
      }}
    >
      <List sx={{ width: '100%' }}>
        {items.map(({ href, icon: Icon }) => {
          const active = matchPath({ path: href, end: true }, location.pathname);

          return (
            <ListItem
              disablePadding
              component={RouterLink}
              key={href}
              to={href}
              sx={{
                flexDirection: 'column',
                color: active ? 'primary.main' : 'text.secondary',
                px: 1,
                py: 0.6,
                '&:hover': {
                  color: 'primary.main'
                }
              }}
            >
              <ListItemIcon
                sx={{
                  minWidth: 'auto',
                  color: '#fff',
                  width: 46,
                  height: 46,
                }}
              >
                <Icon sx={{ fontSize: 40 }} />
              </ListItemIcon>
            </ListItem>
          );
        })}
      </List>
    </Drawer>
  );
};
