import { AppBar, Toolbar } from '@mui/material';

export const Navbar = () => (
  <AppBar
    elevation={0}
    sx={{
      backgroundColor: '#333',
      // borderBottom: '2px solid green'
    }}
  >
    <Toolbar
      disableGutters
      sx={{
        alignItems: 'center',
        display: 'flex',
        minHeight: 44,
        px: 3,
        py: 1,
      }}
    >
      <p style={{ textAlign: 'center', color: '#fff' }}>
        <b>
          Remote Patient Monitoring Dashboard
        </b>
      </p>
    </Toolbar>
  </AppBar>
);
