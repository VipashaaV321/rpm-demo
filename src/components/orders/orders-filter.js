import PropTypes from 'prop-types';
import {
  Box,
  Button,
} from '@mui/material';
import { Adjustments as AdjustmentsIcon } from '../../icons/adjustments';
import { Query } from '../query';

export const OrdersFilter = (props) => {
  const { onQueryChange, query } = props;

  return (
    <Box
      sx={{
        alignItems: 'center',
        display: 'grid',
        gap: 2,
        gridTemplateColumns: {
          sm: '1fr auto',
          xs: 'auto'
        },
        justifyItems: 'flex-start',
        p: 2
      }}
    >
      <Query
        onChange={onQueryChange}
        sx={{
          order: {
            sm: 2,
            xs: 1
          }
        }}
        value={query}
      />
      <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          order: 3
        }}
      >
        <Button
          color="primary"
          startIcon={<AdjustmentsIcon />}
          size="large"
          variant="outlined"
          sx={{
            order: 3,
            m: 1
          }}
        >
          Filter By
        </Button>
        <p>Total Patient : 150</p>
      </Box>
    </Box>
  );
};

OrdersFilter.propTypes = {
  onQueryChange: PropTypes.func,
  query: PropTypes.string
};
