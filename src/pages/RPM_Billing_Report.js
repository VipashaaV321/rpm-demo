import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TablePagination from '@mui/material/TablePagination';
import Chip from '@mui/material/Chip';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';

const BillingReport = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="RPM Billing Report" sx={{ background: '#F0F8FF' }} />
        <Chip label="Filter" variant="outlined" color="success" icon={<ArrowUpwardIcon />} sx={{ m: 1 }} />
        <br />
        <FormControl>
          <TextField id="outlined-basic" label="Billing Code" variant="outlined" defaultValue="CPT 99453" sx={{ m: 2 }} />
          <TextField id="outlined-basic" label="Status" variant="outlined" sx={{ m: 2 }} defaultValue="To Be Billed" />
          <Divider />
          <br />
          <Stack spacing={2} direction="row" sx={{ m: 2 }}>
            Billing Period :
            <br />
            <TextField id="outlined-basic" label="from :" variant="outlined" defaultValue="1/1/2022" />
            <TextField id="outlined-basic" label="to :" variant="outlined" defaultValue="1/1/2022" />
          </Stack>
        </FormControl>
        <CardContent>
          <Divider />
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardContent>
              <TableContainer>
                <Table sx={{ minWidth: 650 }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align="right">Patient Name</TableCell>
                      <TableCell align="right">DOB</TableCell>
                      <TableCell align="right">MRN</TableCell>
                      <TableCell align="right">Payor Insurance</TableCell>
                      <TableCell align="right">Setup Date</TableCell>
                      <TableCell align="right">Device Active for</TableCell>
                      <TableCell align="right">Upload</TableCell>
                      <TableCell align="right">Send to Super Bill</TableCell>
                      <TableCell align="right">Status</TableCell>
                      <TableCell align="right">Send Alert</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell align="right">ABc</TableCell>
                      <TableCell align="right">2/2/1991</TableCell>
                      <TableCell component="th" scope="row" align="right">
                        1234
                      </TableCell>
                      <TableCell align="right">XYZ</TableCell>
                      <TableCell align="right">1/1/12</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">Out Reach for Appoinment</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10]}
                component="div"
                count={10}
                rowsPerPage={5}
                page={0}
              />
              <Divider />
              <Button variant="contained" sx={{ m: 2 }} href="/dashboard">Close</Button>
            </CardContent>
          </Card>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default BillingReport;
