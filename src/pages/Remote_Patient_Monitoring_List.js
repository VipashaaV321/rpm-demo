import {
  Box, Button, Card, Container, Divider, Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableContainer,
  CardHeader,
  CardContent
} from '@mui/material';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { OrdersFilter } from '../components/orders/orders-filter';

export const RemotePatientMonitoringList = () => (
  <>
    <Box
      sx={{
        backgroundColor: 'background.default',
        pb: 3,
        pt: 8
      }}
    >

      <Container maxWidth="lg">
        <Card
          variant="outlined"
        >
          <CardHeader title=" Remote Patient Monitoring Patient List" sx={{ background: '#F0F8FF' }} />
          <Divider />
          <CardContent>
            <Box
              sx={{
                alignItems: 'center',
                display: 'flex',
                mb: 3
              }}
            >
              <TableContainer>
                <FormControl sx={{
                  m: 1,
                }}
                >
                  <FormLabel id="demo-row-radio-buttons-group-label">RPM Status</FormLabel>
                  <RadioGroup
                    row
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name="row-radio-buttons-group"
                  >
                    <FormControlLabel
                      value="Elligible"
                      control={<Radio />}
                      label="Elligible"
                    />
                    <FormControlLabel
                      value="Enrolled"
                      control={<Radio />}
                      label="Enrolled"
                    />
                    <FormControlLabel
                      value="Denied by Patient"
                      control={<Radio />}
                      label="Denied by Patient"
                    />
                  </RadioGroup>
                </FormControl>
                <Box sx={{
                  flexGrow: 1,
                }}
                />
                <Button
                  color="primary"
                  size="large"
                  variant="contained"
                  sx={{
                    m: 1,
                  }}
                  href="/remote-patient-monitoring-list"
                >
                  Refresh Credit
                </Button>
                <Button
                  color="primary"
                  size="large"
                  variant="contained"
                  sx={{
                    m: 1,
                  }}
                  href="/remote-patient-list-editor"
                >
                  Edit Criteria
                </Button>
                <Button
                  color="primary"
                  size="large"
                  variant="contained"
                  sx={{
                    m: 1,
                  }}
                >
                  Export
                </Button>
                <Button
                  color="primary"
                  size="large"
                  variant="contained"
                  sx={{
                    m: 1,
                  }}
                  href="/dashboard"
                >
                  close
                </Button>
              </TableContainer>
            </Box>
            <Card variant="outlined">
              <OrdersFilter />
              <Divider />
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell align="left">Patient Name</TableCell>
                      <TableCell align="right">DOB</TableCell>
                      <TableCell align="right">MRN</TableCell>
                      <TableCell align="right">Primary Payor</TableCell>
                      <TableCell align="right">Chronic Condition</TableCell>
                      <TableCell align="right">Last Visit</TableCell>
                      <TableCell align="right">Conent Signed on</TableCell>
                      <TableCell align="right">RPM Status</TableCell>
                      <TableCell align="center">Action</TableCell>
                      <TableCell align="right">Setup done on</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell align="left">XYZ</TableCell>
                      <TableCell align="right">02/03/1988</TableCell>
                      <TableCell align="right">1234</TableCell>
                      <TableCell align="right">Highmark Medicare</TableCell>
                      <TableCell align="right">I10-Highpertension</TableCell>
                      <TableCell align="right">1/2/22</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">Enrolled</TableCell>
                      <TableCell align="right">
                        <a href="/patient-enroll">Enroll Now</a>
                        / Out reach for appoinment/ SMS
                      </TableCell>
                      <TableCell align="right">{' '}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="left">
                        <a href="/patient-home">ABC</a>
                      </TableCell>
                      <TableCell align="right">02/03/1988</TableCell>
                      <TableCell align="right">1234</TableCell>
                      <TableCell align="right">Highmark Medicare</TableCell>
                      <TableCell align="right">I10-Highpertension</TableCell>
                      <TableCell align="right">1/2/22</TableCell>
                      <TableCell align="right">
                        <a href="/consent-form">1/12/21</a>
                      </TableCell>
                      <TableCell align="right">Enrolled</TableCell>
                      <TableCell align="right">
                        {' '}
                      </TableCell>
                      <TableCell align="right">{' '}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
                <Divider />
              </TableContainer>
              <Divider />
            </Card>
          </CardContent>
        </Card>
      </Container>
    </Box>
  </>
);
