import { Box, Container, Grid } from '@mui/material';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import CloseIcon from '@mui/icons-material/Close';
import PrintIcon from '@mui/icons-material/Print';
import AttachmentIcon from '@mui/icons-material/Attachment';

export const PatientEnroll = () => (
  <Box sx={{ backgroundColor: 'background.default' }}>
    <Container
      maxWidth="lg"
      sx={{
        px: 3,
        py: 10,
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column'
      }}
    >
      <h2>Patient Enrollment: </h2>

      <Grid
        container
        direction="row"
        alignItems="center"
        sx={{
          border: '1px solid #333',
          height: '100px',
          width: '300px',
          m: 2
        }}
      >
        <ul>
          <li>
            Verbal Consent given
          </li>
          <li>
            <a href="/patient-home">
              Signed Consent
            </a>
          </li>

        </ul>
      </Grid>
      <Grid
        container
        direction="row"
        // justifyContent="center"
        alignItems="center"
        sx={{
          border: '1px solid #333',
          height: '150px',
          width: '300px',
          m: 2,
          p: 2
        }}
      >
        <List aria-label="secondary mailbox folder">
          <ListItemButton>
            <a href="consent-form">
              <ListItemText primary="Electronic" />
            </a>
          </ListItemButton>
          <ListItemButton>
            <ListItemText primary="Physical" />
          </ListItemButton>
        </List>
      </Grid>
      <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          mb: 3
        }}
      >
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
          sx={{
            border: '1px solid #333',
            height: '100px',
            width: '300px',
            m: 2
          }}
        >
          <Tabs aria-label="icon label tabs example">
            <Tab icon={<PrintIcon />} label="PRINT" />
            <Tab icon={<AttachmentIcon />} label="ATTACH" />
            <Tab icon={<CloseIcon />} label="CLOSE" href="/dashboard" />
          </Tabs>
        </Grid>
      </Box>
    </Container>
  </Box>

);
