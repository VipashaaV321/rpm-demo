import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, Typography } from '@mui/material';
import Container from '@mui/material/Container';
// import Grid from '@mui/material/Stack';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

const PatientRegistration = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="Patient Registration" sx={{ background: '#F0F8FF' }} />
        <CardContent>
          <Typography sx={{ m: 2 }}>Basic Details</Typography>
          <Grid container spacing={2} sx={{ m: 2 }}>
            <TextField id="outlined-basic" label="Title" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="First Name" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Middle Name" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Last Name" variant="outlined" sx={{ m: 1 }} />
          </Grid>
          <Grid container spacing={2} sx={{ m: 2 }}>
            <TextField id="outlined-basic" label="DOB" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Age" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Sex" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="MRN" variant="outlined" sx={{ m: 1 }} />
          </Grid>
          <Divider />
          <Grid container spacing={2} sx={{ m: 2 }}>
            <TextField id="outlined-basic" label="Patient Race" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Ethinity" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Preffered Language" variant="outlined" sx={{ m: 1 }} />
          </Grid>
          <Divider />
          <Typography container sx={{ m: 2 }}>Contact Details</Typography>
          <Grid spacing={2} sx={{ m: 2 }}>
            <TextField id="outlined-basic" label="Address 1" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Address 2" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="zip" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="City" variant="outlined" sx={{ m: 1 }} />
          </Grid>
          <Grid container spacing={2} sx={{ m: 2 }}>
            <TextField id="outlined-basic" label="State" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Country" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Cell Number" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Office Number" variant="outlined" sx={{ m: 1 }} />
          </Grid>
          <Grid container spacing={2} sx={{ m: 2 }}>
            <TextField id="outlined-basic" label="Home Number" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Primary Contact" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Email" variant="outlined" sx={{ m: 1 }} />
          </Grid>
          <Divider />
          <Typography sx={{ m: 2 }}>Payor Information</Typography>
          <Grid container spacing={3} sx={{ m: 2 }}>
            <TextField id="outlined-basic" label="Primary Insurance" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Secondary Insurance" variant="outlined" sx={{ m: 1 }} />
          </Grid>
          <Divider />
          <Typography sx={{ m: 2 }}>Care Team Information</Typography>
          <Typography sx={{ m: 2 }}>PCP : Primary Point of Contact</Typography>
          <Grid container spacing={2} sx={{ m: 2 }}>
            <TextField id="outlined-basic" label="Last Name" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="First Name" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Contact Number" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Email" variant="outlined" sx={{ m: 1 }} />
          </Grid>
          <Typography sx={{ m: 2 }}>Care Team Member-1 : Primary Point of Contact</Typography>
          <Grid container spacing={2} sx={{ m: 2 }}>
            <TextField id="outlined-basic" label="Last Name" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="First Name" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Contact Number" variant="outlined" sx={{ m: 1 }} />
            <TextField id="outlined-basic" label="Email" variant="outlined" sx={{ m: 1 }} />
          </Grid>
          <FormControl sx={{ m: 2 }}>
            <FormLabel id="demo-row-radio-buttons-group-label">Communication Preference</FormLabel>
            <RadioGroup
              row
              aria-labelledby="demo-row-radio-buttons-group-label"
              name="row-radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="No preference" />
              <FormControlLabel value="male" control={<Radio />} label="By Email" />
              <FormControlLabel value="other" control={<Radio />} label="By Phone" />
              <FormControlLabel
                value="disabled"
                control={<Radio />}
                label="By SMS"
              />
            </RadioGroup>
          </FormControl>
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardContent>
              <Button variant="contained" sx={{ m: 1 }} href="/patient-home">Save</Button>
              <Button variant="contained" sx={{ m: 1 }} href="/device-manager">Add Device</Button>
              <Button variant="contained" sx={{ m: 1 }} href="/patient-registration">Edit</Button>
              <Button variant="contained" sx={{ m: 1 }} href="/dashboard">Exit</Button>
            </CardContent>
          </Card>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default PatientRegistration;
