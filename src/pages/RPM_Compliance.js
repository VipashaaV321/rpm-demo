import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Checkbox from '@mui/material/Checkbox';

function createData(patient, noncompliancedays, lastreadingtaken, takeaction) {
  return { patient, noncompliancedays, lastreadingtaken, takeaction };
}

const rows = [
  createData(' ', ' ', ' ', 'Send Care Team/ Patient '),
  createData(' ', ' ', ' ', 'Send Care Team '),
];

const RPMCompliance = () => (
  <Container fixed sx={{ p: 3 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="RPM Compliance" sx={{ background: '#F0F8FF' }} />
        <Divider />
        <CardContent>
          <TableContainer>
            <Stack spacing={2} direction="row" sx={{ m: 2 }}>
              <Button variant="outlined" href="/rpm-compliance">Refresh</Button>
              <Button variant="outlined">Print List</Button>
              <Button variant="outlined">Export to Excel</Button>
              <Button variant="outlined" href="/dashboard">Close</Button>
            </Stack>
          </TableContainer>
          <Divider />
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardContent>
              <TableContainer>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center"><Checkbox defaultChecked /></TableCell>
                      <TableCell>Patient</TableCell>
                      <TableCell align="right">Non compliance days</TableCell>
                      <TableCell align="right">Last reading taken</TableCell>
                      <TableCell align="right">Take action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map((row) => (
                      <TableRow
                        key={row.patient}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell align="center"><Checkbox defaultChecked /></TableCell>
                        <TableCell component="th" scope="row">
                          {row.patient}
                        </TableCell>
                        <TableCell align="right">{row.noncompliancedays}</TableCell>
                        <TableCell align="right">{row.lastreadingtaken}</TableCell>
                        <TableCell align="right">{row.takeaction}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </CardContent>
          </Card>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default RPMCompliance;
