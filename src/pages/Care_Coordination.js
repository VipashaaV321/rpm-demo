import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TablePagination from '@mui/material/TablePagination';
import Chip from '@mui/material/Chip';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';

const CareCoordination = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="Care Coordination" sx={{ background: '#F0F8FF' }} />
        <Divider />
        <CardContent>
          <TableContainer>
            <Stack spacing={2} direction="row" sx={{ m: 2 }}>
              <Button variant="outlined">Refresh</Button>
              <Button variant="outlined">Print List</Button>
              <Button variant="outlined">Export to Excel</Button>
              <Button variant="outlined" href="/dashboard">Close</Button>
            </Stack>
          </TableContainer>
          <Divider />
          <p>Filter:</p>
          <TableContainer>
            <Stack spacing={2} direction="row" sx={{ m: 2 }}>
              <Chip label="Physician" variant="outlined" color="success" />
              <Chip label="Care Manager" variant="outlined" color="success" />
              <Chip label="Call Status" variant="outlined" color="success" />
              <Chip label="Current Month" variant="outlined" color="success" icon={<ArrowUpwardIcon />} />
            </Stack>
          </TableContainer>
          <Divider />
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardContent>
              <TableContainer>
                <Table sx={{ minWidth: 650 }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align="right">MRN</TableCell>
                      <TableCell align="right">Patient Name</TableCell>
                      <TableCell align="right">DOB</TableCell>
                      <TableCell align="right">PCP</TableCell>
                      <TableCell align="right">Care Manager</TableCell>
                      <TableCell align="right">Enroll Date</TableCell>
                      <TableCell align="right">Initial CP</TableCell>
                      <TableCell align="right">MinsAchieved</TableCell>
                      <TableCell align="right">Goal</TableCell>
                      <TableCell align="right">Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell component="th" scope="row" align="right">
                        1
                      </TableCell>
                      <TableCell align="right"><Button variant="text" href="/patient-dashboard">KEVIN M</Button></TableCell>
                      <TableCell align="right">2/2/1991</TableCell>
                      <TableCell align="right">XYZ</TableCell>
                      <TableCell align="right">ABS</TableCell>
                      <TableCell align="right">1/1/12</TableCell>
                      <TableCell align="right">1/8/22</TableCell>
                      <TableCell align="right">15 mins</TableCell>
                      <TableCell align="right">20 mins</TableCell>
                      <TableCell align="right"><Button variant="text" href="/patient-dashboard">Call</Button></TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10]}
                component="div"
                count={10}
                rowsPerPage={5}
                page={0}
              />
            </CardContent>
          </Card>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default CareCoordination;
