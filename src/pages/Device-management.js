import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TablePagination from '@mui/material/TablePagination';
import Chip from '@mui/material/Chip';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';

const DeviceManagement = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="Device Manager" sx={{ background: '#F0F8FF' }} />
        <Divider />
        <CardContent>
          <TableContainer>
            <Stack spacing={2} direction="row" sx={{ m: 2 }}>
              <Chip label="Total Patients : 150" variant="outlined" color="success" />
              <Chip label="Device" variant="outlined" color="success" />
              <Chip label="Consent Missing" variant="outlined" color="success" />
              <Chip label="Filter By: Payor" variant="outlined" color="success" icon={<ArrowUpwardIcon />} />
            </Stack>
          </TableContainer>
          <Divider />
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardContent>
              <TableContainer>
                <Table sx={{ minWidth: 650 }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align="right">Patient Name</TableCell>
                      <TableCell align="right">DOB</TableCell>
                      <TableCell align="right">MRN</TableCell>
                      <TableCell align="right">Device given</TableCell>
                      <TableCell align="right">Device IMEI</TableCell>
                      <TableCell align="right">Last Vital Action</TableCell>
                      <TableCell align="right">Action</TableCell>
                      <TableCell align="right">Note</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell align="right">ABc</TableCell>
                      <TableCell align="right">2/2/1991</TableCell>
                      <TableCell component="th" scope="row" align="right">
                        1234
                      </TableCell>
                      <TableCell align="right">1/1/12</TableCell>
                      <TableCell align="right"><a href="/device-manager">12399900</a></TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">Out Reach for Appoinment</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10]}
                component="div"
                count={10}
                rowsPerPage={5}
                page={0}
              />
              <Divider />
              <Button variant="contained" href="/dashboard" sx={{ m: 2 }}>Close</Button>
            </CardContent>
          </Card>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default DeviceManagement;
