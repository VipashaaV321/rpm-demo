import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TablePagination from '@mui/material/TablePagination';
import Chip from '@mui/material/Chip';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';

const CareTeam = () => {
  const [value, setValue] = React.useState('');

  const handleRadioChange = (event) => {
    setValue(event.target.value);
  };
  return (
    <Container fixed sx={{ p: 5 }}>
      <Grid
        item
        xs={12}
      >
        <Card
          variant="outlined"
        >
          <CardHeader title="Care Team" sx={{ background: '#F0F8FF' }} />
          <Chip label="Filter" variant="outlined" color="success" icon={<ArrowUpwardIcon />} sx={{ m: 2 }} />
          <br />
          <FormControl sx={{ m: 2 }}>
            Status:
            <RadioGroup
              row
              aria-labelledby="demo-row-radio-buttons-group-label"
              name="row-radio-buttons-group"
              value={value}
              onChange={handleRadioChange}
            >
              <FormControlLabel value="Assigned" control={<Radio />} label="Assigned" />
              <FormControlLabel value="Unassigned" control={<Radio />} label="Unassigned" />

            </RadioGroup>
          </FormControl>
          <br />
          <TextField id="outlined-basic" label="PCP" variant="outlined" sx={{ m: 2 }} />
          <TextField id="outlined-basic" label="Care Manager" variant="outlined" sx={{ m: 2 }} />
          <Divider />

          <CardContent>
            <Stack spacing={2} direction="row" sx={{ m: 2 }}>
              <Chip label="Total Patients : 150" variant="outlined" color="success" />

            </Stack>
            <Divider />
            <Card
              variant="outlined"
              sx={{
                mt: 3,
              }}
            >
              <CardContent>
                <TableContainer>
                  <Table sx={{ minWidth: 650 }}>
                    <TableHead>
                      <TableRow>
                        <TableCell align="right">
                          <Radio />
                        </TableCell>
                        <TableCell align="right">Patient Name</TableCell>
                        <TableCell align="right">DOB</TableCell>
                        <TableCell align="right">MRN</TableCell>
                        <TableCell align="right">Primary Payor</TableCell>
                        <TableCell align="right">Chronic Condition</TableCell>
                        <TableCell align="right">Consent Signed on</TableCell>
                        <TableCell align="right">Devices</TableCell>
                        <TableCell align="right">Last Vital Action</TableCell>
                        <TableCell align="right">Action</TableCell>
                        <TableCell align="right">Follow Up Date</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell align="right">
                          <Radio />
                        </TableCell>
                        <TableCell align="right">ABc</TableCell>
                        <TableCell align="right">2/2/1991</TableCell>
                        <TableCell component="th" scope="row" align="right">
                          1234
                        </TableCell>
                        <TableCell align="right">XYZ</TableCell>
                        <TableCell align="right">Essential Hypertension</TableCell>
                        <TableCell align="right">1/1/12</TableCell>
                        <TableCell align="right">{' '}</TableCell>
                        <TableCell align="right">{' '}</TableCell>
                        <TableCell align="right">Out Reach for Appoinment</TableCell>
                        <TableCell align="right">{' '}</TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
                <TablePagination
                  rowsPerPageOptions={[5, 10]}
                  component="div"
                  count={10}
                  rowsPerPage={5}
                  page={0}
                />
                <Divider />
                {
                  value === 'Assigned' ? (
                    <>
                      <Button variant="contained" sx={{ m: 2 }}>Change PCP</Button>
                      <Button variant="contained">Change Care Manager</Button>
                    </>
                  ) : (
                    <>
                      <Button variant="contained" sx={{ m: 2 }}>Assign Care Team</Button>
                    </>
                  )
                }
              </CardContent>
            </Card>
          </CardContent>
        </Card>
      </Grid>
    </Container>
  );
};

export default CareTeam;
