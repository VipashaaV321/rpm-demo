import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import Checkbox from '@mui/material/Checkbox';
import TextField from '@mui/material/TextField';

const NewFollowup = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="New Follow Up" sx={{ background: '#F0F8FF' }} />
        <Divider />
        <CardContent
          sx={{ p: 3 }}
        >
          Follow Up Type
          <FormGroup>
            <FormControlLabel control={<Checkbox defaultChecked />} label="Label" />
            <FormControlLabel control={<Checkbox />} label="Disabled" />
            <FormControlLabel control={<Checkbox />} label="Label" />
            <FormControlLabel control={<Checkbox />} label="Disabled" />
          </FormGroup>
          <Divider />

          <FormGroup sx={{ mt: 3 }}>
            <TextField
              id="outlined"
              label="Subject"
              sx={{ mt: 2, width: '30ch' }}
            />
            <TextField
              id="outlined"
              label="Patient"
              sx={{ mt: 2, width: '30ch' }}
            />
            <TextField
              id="outlined"
              label="Follow Up"
              sx={{ mt: 2, width: '30ch' }}
            />
            <TextField
              id="outlined"
              label="Assign to"
              sx={{ mt: 2, width: '30ch' }}
            />
            <TextField
              label="Notes"
              id="standard-basic"
              variant="standard"
              sx={{ mt: 2, width: '30ch' }}
            />
          </FormGroup>
          <Divider sx={{ m: 2 }} />

          <Stack spacing={2} direction="row">
            <Button variant="contained" href="/dashboard">Save</Button>
            <Button variant="contained">Close</Button>
          </Stack>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default NewFollowup;
