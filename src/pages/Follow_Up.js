import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TablePagination from '@mui/material/TablePagination';
import Chip from '@mui/material/Chip';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';

const Followup = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="Follow Up" sx={{ background: '#F0F8FF' }} />
        <TableContainer>
          <Stack spacing={2} direction="row" sx={{ m: 1 }}>
            <Button variant="contained" href="/new-followup">New</Button>
            <Button variant="contained">Print</Button>
            <Button variant="contained">Delete</Button>
            <Button variant="contained" href="/dashboard">Close</Button>
          </Stack>
        </TableContainer>
        <Divider />
        <Chip label="Filter" variant="outlined" color="success" icon={<ArrowUpwardIcon />} sx={{ m: 2 }} />
        <br />
        <Stack spacing={2} direction="row" sx={{ m: 2 }}>
          <TextField id="outlined-basic" label="from :" variant="outlined" defaultValue="1/1/2022" />
          <TextField id="outlined-basic" label="to :" variant="outlined" defaultValue="1/1/2022" />
        </Stack>
        <FormControl sx={{ m: 2 }}>
          Follow up Date
          <RadioGroup
            row
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
          >
            <FormControlLabel value="Assigned" control={<Radio />} label="Today's" />
            <FormControlLabel value="Unassigned" control={<Radio />} label="Last 7 Days" />
            <FormControlLabel value="Unassigned" control={<Radio />} label="30 Days" />

          </RadioGroup>
        </FormControl>
        <FormControl sx={{ m: 2 }}>
          Patient :
          <RadioGroup
            row
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
          >
            <FormControlLabel value="Assigned" control={<Radio />} label="All Patients" />
            <FormControlLabel value="Unassigned" control={<Radio />} label="Patients" />

          </RadioGroup>
        </FormControl>
        <FormControl sx={{ m: 2 }}>
          Created By:
          <RadioGroup
            row
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
          >
            <FormControlLabel value="Assigned" control={<Radio />} label="All Creators" />
            <FormControlLabel value="Unassigned" control={<Radio />} label="Creators" />

          </RadioGroup>
        </FormControl>
        <FormControl sx={{ m: 2 }}>
          Status:
          <RadioGroup
            row
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
          >
            <FormControlLabel value="Assigned" control={<Radio />} label="Pending" />
            <FormControlLabel value="Unassigned" control={<Radio />} label="Working" />
            <FormControlLabel value="Unassigned" control={<Radio />} label="Voicemail" />
            <FormControlLabel value="Unassigned" control={<Radio />} label="Deleted" />
            <FormControlLabel value="Unassigned" control={<Radio />} label="Completed" />
          </RadioGroup>
        </FormControl>
        <br />
        <TextField id="filled-basic" label="Assigned to" variant="filled" sx={{ m: 2 }} defaultValue="All" />
        <TextField id="filled-basic" label="Location" variant="filled" sx={{ m: 2 }} defaultValue="All" />
        <Divider />

        <CardContent>
          {/* <Stack spacing={2} direction="row" sx={{ m: 2 }}>
              <Chip label="Total Patients : 150" variant="outlined" color="success" />

            </Stack> */}
          <Divider />
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardContent>
              <TableContainer>
                <Table sx={{ minWidth: 650 }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align="right">Patient Name</TableCell>
                      <TableCell align="right">Follow up Date</TableCell>
                      <TableCell align="right">Phone</TableCell>
                      <TableCell align="right">Created By</TableCell>
                      <TableCell align="right">Assigned to</TableCell>
                      <TableCell align="right">Last Action</TableCell>
                      <TableCell align="right">Status</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
              <Divider />
              <TablePagination
                rowsPerPageOptions={[5, 10]}
                component="div"
                count={10}
                rowsPerPage={5}
                page={0}
              />
              <Divider />
              {/* {
                  value === 'Assigned' ? (
                    <>
                      <Button variant="contained" sx={{ m: 2 }}>Change PCP</Button>
                      <Button variant="contained">Change Care Manager</Button>
                    </>
                  ) : (
                    <>
                      <Button variant="contained" sx={{ m: 2 }}>Assign Care Team</Button>
                    </>
                  )
                } */}
            </CardContent>
          </Card>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default Followup;
