import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

const AlertPage = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="Alert" sx={{ background: '#F0F8FF' }} />
        <Divider />
        <CardContent>
          <Divider />
          <FormControl sx={{ m: 1 }}>
            <RadioGroup
              row
              aria-labelledby="demo-row-radio-buttons-group-label"
              name="row-radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="All Patient" />
              <FormControlLabel value="male" control={<Radio />} label="Patient" />
            </RadioGroup>
          </FormControl>
          <br />
          <FormControl sx={{ m: 1 }}>
            <RadioGroup
              row
              aria-labelledby="demo-row-radio-buttons-group-label"
              name="row-radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="Today's" />
              <FormControlLabel value="male" control={<Radio />} label="Last 7 day's" />
              <FormControlLabel value="other" control={<Radio />} label="30 day's" />
            </RadioGroup>
            <p>   from ____________________ to _________________________</p>
          </FormControl>
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardContent>
              <nav aria-label="secondary mailbox folders">
                <List>
                  <ListItem disablePadding>
                    <ListItemButton href="patient-communication">
                      <ListItemText primary="1. Patient XYZ has a BP reading 160/70 action needed" secondary="Check Care Plan / Phone Call / SMS" />
                    </ListItemButton>
                  </ListItem>
                  <ListItem disablePadding>
                    <ListItemButton component="a">
                      <ListItemText primary="2. Patient" />
                    </ListItemButton>
                  </ListItem>
                  <ListItem disablePadding>
                    <ListItemButton component="a">
                      <ListItemText primary="3. Patient" />
                    </ListItemButton>
                  </ListItem>
                </List>
              </nav>
            </CardContent>
          </Card>
          <Stack spacing={2} direction="row" sx={{ m: 2 }}>
            <Button variant="outlined" href="/dashboard">Close</Button>
          </Stack>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default AlertPage;
