import { Box, Container, Grid } from '@mui/material';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

export const NewDevice = () => (
  <Box sx={{ backgroundColor: 'background.default' }}>
    <Container
      maxWidth="lg"
      sx={{
        px: 3,
        py: 12,
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column'
      }}
    >
      <Grid
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
        sx={{
          border: '1px solid #333',
          height: '400px',
          width: '350px',
        }}
      >
        <h2>New Device: </h2>
        <Stack
          component="form"
          sx={{
            width: '25ch',
          }}
          spacing={2}
          noValidate
          autoComplete="off"
        >
          <TextField fullWidth id="outlined-basic" label="Device Type" variant="outlined" />
          <TextField fullWidth id="outlined-basic" label="Device IMEI Number." variant="outlined" />
          <Button variant="contained" color="success" size="large" href="device-manager">
            Save
          </Button>

        </Stack>
      </Grid>
    </Container>
  </Box>
);
