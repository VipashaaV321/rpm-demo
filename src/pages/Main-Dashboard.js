import { Helmet } from 'react-helmet';
import { PieChart } from 'react-minimal-pie-chart';
import { Box, Card, Container, Divider, Grid, Typography, Button, TextField } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
// import Input from '@mui/material/Input';
import SearchIcon from '@mui/icons-material/Search';
import AssignmentIndOutlinedIcon from '@mui/icons-material/AssignmentIndOutlined';
import InputAdornment from '@mui/material/InputAdornment';
import { SummaryItem } from '../components/reports/summary-item';
import { PerformanceIndicators } from '../components/reports/performance-indicators';
import DashboardCard from '../components/dashboard_card';
// CardHeader,
const stats = [
  {
    content: '100',
    icon: AssignmentIndOutlinedIcon,
    label: 'Eligible',
    href: '/remote-patient-monitoring-list'
  },
  {
    content: '10',
    icon: AssignmentIndOutlinedIcon,
    label: 'Enrolled',
    href: '/patient-enroll'
  },
  {
    content: '2',
    icon: AssignmentIndOutlinedIcon,
    label: 'Abnormal Today',
    href: '/abnormal-reading'
  },
  {
    content: '1',
    icon: AssignmentIndOutlinedIcon,
    label: 'Non Compliant',
    href: '/rpm-compliance'
  },
  {
    content: '5',
    icon: AssignmentIndOutlinedIcon,
    label: 'Ready To Bill',
    href: '/rpm-billing-report'
  },
  {
    content: '5',
    icon: AssignmentIndOutlinedIcon,
    label: 'Follow up due',
    href: '/follow-up'
  }
];

export const MainDashboard = () => (
  <>
    <Grid
      container
    >
      <Grid
        item
        md={2}
        xs={12}
        style={{ borderRight: '1px solid #ccc' }}
        sx={{ p: 1 }}
      >
        <DashboardCard />
      </Grid>
      <Grid
        item
        md={10}
        xs={12}
      >
        <Helmet>
          <title> RPM-Dashboard</title>
        </Helmet>
        <Box
          sx={{
            backgroundColor: 'background.default',
            pb: 3,
            pt: 8
          }}
        >
          <Container maxWidth="lg">
            <Grid
              container
              spacing={3}
            >
              {stats.map((item) => (
                <Grid
                  item
                  key={item.label}
                  md={4}
                  xs={12}
                >
                  <SummaryItem
                    content={item.content}
                    icon={item.icon}
                    label={item.label}
                    href={item.href}
                  />
                </Grid>
              ))}
              <Grid
                item
                xs={12}
              >
                <TextField
                  fullWidth
                  id="outlined-basic"
                  sx={{ mb: 2 }}
                  label="Patient Search"
                  startAdornment={<InputAdornment position="start"><SearchIcon /></InputAdornment>}
                />
                <PerformanceIndicators />
              </Grid>
              <Grid
                item
                xs={12}
              >
                <Grid
                  container
                  spacing={3}
                >
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <Card
                      sx={{
                        display: 'grid',
                        gap: 3,
                        gridAutoFlow: 'row',
                        p: 3
                      }}
                      variant="outlined"
                    >
                      <Button href="/rpm-billing-report">
                        <Typography
                          color="textPrimary"
                          sx={{ mb: 1, textAlign: 'left' }}
                          variant="h6"
                        >
                          RPM billing History
                        </Typography>
                      </Button>
                      <Divider />
                      <TableContainer>
                        <PieChart
                          style={{
                            width: '250px',
                            height: '250px'
                          }}
                          // label="bill"
                          data={[
                            { title: 'One', value: 10, color: '#E38627' },
                            { title: 'Two', value: 15, color: '#C13C37' },
                            { title: 'Three', value: 20, color: '#6A2135' },
                            { title: 'Three', value: 27, color: '#FFD700' },
                          ]}
                        />
                      </TableContainer>
                    </Card>
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <Card
                      sx={{
                        display: 'grid',
                        gap: 3,
                        gridAutoFlow: 'row',
                        p: 3
                      }}
                      variant="outlined"
                    >
                      <Typography
                        color="textPrimary"
                        variant="h6"
                      >
                        Care coordination Time Spent
                        <Button href="/care-coordination"> This Month</Button>
                      </Typography>
                      <Divider />
                      <TableContainer>
                        <PieChart
                          style={{
                            width: '250px',
                            height: '257px'
                          }}
                          data={[
                            { title: 'One', value: 10, color: '#66CDAA' },
                            { title: 'Two', value: 15, color: '#0059b3' },
                            { title: 'One', value: 17, color: '#1a8cff' },
                            { title: 'Two', value: 20, color: '#20B2AA' },
                            { title: 'Three', value: 25, color: '#003366' },
                          ]}
                        />
                      </TableContainer>
                    </Card>
                  </Grid>
                </Grid>

                <Grid
                  container
                  spacing={3}
                  sx={{
                    mt: 1
                  }}
                >
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <Card
                      sx={{
                        display: 'grid',
                        gap: 3,
                        gridAutoFlow: 'row',
                        p: 3
                      }}
                      variant="outlined"
                    >
                      <Typography
                        color="textPrimary"
                        variant="h6"
                      >
                        Today: Uploaded Data
                        <Button href="/rpm-monitoring"> View All</Button>
                      </Typography>
                      <Divider />
                      <TableContainer>
                        <Table>
                          <TableHead>
                            <TableRow>
                              <TableCell align="right">Patient Name: Weighting Scale</TableCell>
                              <TableCell align="right">Pulse Ox</TableCell>
                              <TableCell align="right">BP Machine</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            <TableRow>
                              <TableCell align="left">1. ABC Weight: 131</TableCell>
                              <TableCell align="right">Pulse: 98</TableCell>
                              <TableCell component="th" scope="row" align="right">
                                BP: 120/90
                              </TableCell>
                            </TableRow>
                            <TableRow>
                              <TableCell align="left">2. EFG</TableCell>
                              <TableCell align="right">Pulse: 98</TableCell>
                              <TableCell component="th" scope="row" align="right">
                                BP: 120/90
                              </TableCell>
                            </TableRow>
                            <TableRow>
                              <TableCell align="left">3. XYZ</TableCell>
                              <TableCell align="right">Pulse: 98</TableCell>
                              <TableCell component="th" scope="row" align="right">
                                BP: 120/90
                              </TableCell>
                            </TableRow>
                            <TableRow>
                              <TableCell align="left">4. bXYZ</TableCell>
                              <TableCell align="right">Pulse: 98</TableCell>
                              <TableCell component="th" scope="row" align="right">
                                BP: 120/90
                              </TableCell>
                            </TableRow>
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </Card>
                  </Grid>
                  <Grid
                    item
                    md={6}
                    xs={12}
                  >
                    <Card
                      sx={{
                        display: 'grid',
                        gap: 3,
                        gridAutoFlow: 'row',
                        p: 3
                      }}
                      variant="outlined"
                    >
                      <Typography
                        color="textPrimary"
                        variant="h6"
                      >
                        Non-compliance
                        <Button href="/rpm-compliance"> View All</Button>
                      </Typography>
                      <Divider />
                      <TableContainer>
                        <Table>
                          <TableHead>
                            <TableRow>
                              <TableCell align="right">Patient Name</TableCell>
                              <TableCell align="right">Vital Not Taken</TableCell>
                              <TableCell align="right">Last Vital Action</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            <TableRow>
                              <TableCell align="right">ABc</TableCell>
                              <TableCell align="right">1</TableCell>
                              <TableCell component="th" scope="row" align="right">
                                2/2/1991
                              </TableCell>
                            </TableRow>
                          </TableBody>
                        </Table>
                      </TableContainer>
                      <Divider />
                      <Typography
                        color="textPrimary"
                        variant="h6"
                        sx={{
                          mt: 2
                        }}
                      >
                        Today: Alert - Vital
                        <Button href="/abnormal-reading"> View All</Button>
                      </Typography>
                      <Table>
                        <TableHead>
                          <TableRow>
                            <TableCell align="left">Patient Name</TableCell>
                            <TableCell align="right">Pulse Ox</TableCell>
                            <TableCell align="right">BP Machine</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          <TableRow>
                            <TableCell align="left">ABC Weight</TableCell>
                            <TableCell align="right">Pulse: 98</TableCell>
                            <TableCell component="th" scope="row" align="right">
                              BP: 120/90
                            </TableCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                      <Divider />
                    </Card>
                  </Grid>
                </Grid>

              </Grid>
            </Grid>
          </Container>
        </Box>
      </Grid>
    </Grid>
  </>
);
