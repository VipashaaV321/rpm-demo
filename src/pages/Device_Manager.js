import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, Box } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/Inbox';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

function createData(deviceType, imei, status, action) {
  return { deviceType, imei, status, action };
}

const rows = [
  createData('Hub', 15911111, 'Active', 'X'),
];

const DeviceManager = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="Device Manager:" sx={{ background: '#F0F8FF' }} />
        <Divider />
        <CardContent>
          <Box
            sx={{
              gap: 3,
              display: 'grid',
              gridTemplateColumns: {
                md: 'repeat(5, 1fr)',
                sm: 'repeat(2, 1fr)',
                xs: 'repeat(1, 1fr)'
              }
            }}
          >
            <Stack spacing={2} direction="row">
              <Button variant="contained">Refresh</Button>
              <Button variant="contained">Add New Device</Button>
            </Stack>
          </Box>
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardHeader title="Registered Device:" />
            <Divider />
            <CardContent>
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }}>
                  <TableHead>
                    <TableRow>
                      <TableCell>Device Type</TableCell>
                      <TableCell align="right">IMEI Number</TableCell>
                      <TableCell align="right">Status</TableCell>
                      <TableCell align="right">Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map((row) => (
                      <TableRow key={row.deviceType}>
                        <TableCell component="th" scope="row">
                          {row.deviceType}
                        </TableCell>
                        <TableCell align="right">{row.imei}</TableCell>
                        <TableCell align="right">{row.status}</TableCell>
                        <TableCell align="right">{row.action}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </CardContent>
          </Card>
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardHeader title="Vitals to Monitor:" />
            <Divider />
            <CardContent>
              <List>
                <ListItem disablePadding>
                  <ListItemButton>
                    <ListItemIcon>
                      <InboxIcon />
                    </ListItemIcon>
                    <ListItemText primary="Blood Pressure" />
                  </ListItemButton>
                </ListItem>
                <ListItem disablePadding>
                  <ListItemButton>
                    <ListItemIcon>
                      <InboxIcon />
                    </ListItemIcon>
                    <ListItemText primary="SpO2" />
                  </ListItemButton>
                </ListItem>
                <ListItem disablePadding>
                  <ListItemButton>
                    <ListItemIcon>
                      <InboxIcon />
                    </ListItemIcon>
                    <ListItemText primary="Pulse" />
                  </ListItemButton>
                </ListItem>
                <ListItem disablePadding>
                  <ListItemButton>
                    <ListItemIcon>
                      <InboxIcon />
                    </ListItemIcon>
                    <ListItemText primary="HR" />
                  </ListItemButton>
                </ListItem>
                <ListItem disablePadding>
                  <ListItemButton>
                    <ListItemIcon>
                      <InboxIcon />
                    </ListItemIcon>
                    <ListItemText primary="Blood Sugar" />
                  </ListItemButton>
                </ListItem>
                <ListItem disablePadding>
                  <ListItemButton>
                    <ListItemIcon>
                      <InboxIcon />
                    </ListItemIcon>
                    <ListItemText primary="Weight" />
                  </ListItemButton>
                </ListItem>
              </List>
            </CardContent>
          </Card>
        </CardContent>
        <Stack spacing={2} direction="row" sx={{ m: 2 }}>
          <Button variant="outlined">Save</Button>
          <Button variant="outlined">Reset</Button>
          <Button variant="outlined">Cancel</Button>
        </Stack>
      </Card>
    </Grid>
  </Container>
);

export default DeviceManager;
