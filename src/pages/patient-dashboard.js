import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Card, CardHeader, Divider, CardContent, Typography, Box, TableContainer } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import HexagonTwoToneIcon from '@mui/icons-material/HexagonTwoTone';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const PatientDashboard = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <Container fixed sx={{ p: 5 }}>
      <Grid
        item
        xs={12}
      >
        <Card
          variant="outlined"
        >
          <CardHeader title="KEVIN M." sx={{ background: '#F0F8FF' }} />
          <TableContainer>
            <Stack spacing={8} direction="row" sx={{ p: 2 }}>
              <Typography
                color="textPrimary"
              >
                MRN: 1
              </Typography>
              <Typography
                color="textPrimary"
              >
                DOB :
              </Typography>
              <Typography
                color="textPrimary"
              >
                Age :
              </Typography>
              <Typography
                color="textPrimary"
              >
                Gender :
              </Typography>
              <Typography
                color="textPrimary"
              >
                Phone No. :
              </Typography>
              <Typography
                color="textPrimary"
              >
                Email :
              </Typography>
              <Typography
                color="textPrimary"
              >
                Payor :
              </Typography>
            </Stack>
          </TableContainer>
          <Divider />
          <CardContent>
            <Grid
              container
              spacing={3}
              sx={{
                p: 4
              }}
            >
              <Grid
                item
                md={6}
                xs={12}
              >
                <Card
                  sx={{
                    display: 'grid',
                    gap: 3,
                    gridAutoFlow: 'row',
                    p: 3
                  }}
                  variant="outlined"
                >
                  <Typography
                    color="textPrimary"
                    sx={{ mb: 1 }}
                    variant="h6"
                  >
                    Remote Patient Monitoring :
                  </Typography>
                  <Divider />
                  <Typography
                    color="textPrimary"
                    sx={{ mb: 1 }}
                  >
                    Enroll Date :
                  </Typography>
                  <Typography
                    color="textPrimary"
                    sx={{ mb: 1 }}
                  >
                    Chronic Conditions :
                  </Typography>

                  <Typography
                    color="textPrimary"
                    sx={{ mt: 1 }}
                    variant="h6"
                  >
                    Status :
                  </Typography>
                  <Divider />
                  <Typography
                    color="textPrimary"
                    sx={{ mb: 1 }}
                  >
                    Care Manager:
                  </Typography>
                  <Typography
                    color="textPrimary"
                  >
                    Physician :
                  </Typography>
                  <Typography
                    color="textPrimary"
                  >
                    Total RPM Months :
                  </Typography>
                </Card>
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <Card
                  sx={{
                    display: 'grid',
                    gap: 3,
                    gridAutoFlow: 'row',
                    p: 3
                  }}
                  variant="outlined"
                >
                  <TableContainer>
                    <Typography
                      color="textPrimary"
                      sx={{ mb: 1 }}
                      variant="h6"
                    >
                      Timer
                    </Typography>
                    <Divider />
                    <Grid
                      container
                      spacing={1}
                    >
                      <Grid
                        item
                        md={6}
                        xs={12}
                        sx={{ textAlign: 'center' }}
                      >
                        <HexagonTwoToneIcon
                          color="primary"
                          sx={{
                            fontSize: '150px'
                          }}
                        />
                        <Typography
                          color="textPrimary"
                        >
                          Start
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        md={6}
                        xs={12}
                        sx={{ textAlign: 'center' }}
                      >
                        <HexagonTwoToneIcon
                          color="primary"
                          sx={{
                            fontSize: '150px'
                          }}
                        />
                        <Typography
                          color="textPrimary"
                        >
                          End
                        </Typography>
                      </Grid>
                    </Grid>
                    <Typography
                      color="textPrimary"
                      sx={{ textAlign: 'center' }}
                    >
                      Goal : 20 Min
                    </Typography>
                    <Divider />
                    <Stack spacing={7} direction="row" sx={{ mb: 2 }}>
                      <Typography
                        color="textPrimary"
                      >
                        BY : ABC
                      </Typography>
                      <Typography
                        color="textPrimary"
                      >
                        {'        '}
                        Date :
                      </Typography>
                      <Typography
                        color="textPrimary"
                      >
                        Time:
                      </Typography>
                      <Typography
                        color="textPrimary"
                      >
                        Log time:
                      </Typography>
                    </Stack>
                  </TableContainer>
                </Card>
              </Grid>
            </Grid>

            <Divider />

            <Box sx={{ borderBottom: 1, borderColor: 'divider', p: 2 }}>
              <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                <Tab label="Monthly Progress" {...a11yProps(0)} />
                <Tab label="Monthly Review" {...a11yProps(1)} />
              </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
              <Grid
                container
                spacing={3}
                sx={{
                  p: 1
                }}
              >
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <Card
                    sx={{
                      display: 'grid',
                      gap: 3,
                      gridAutoFlow: 'row',
                      p: 3
                    }}
                    variant="outlined"
                  >
                    <Stack spacing={10} direction="row">
                      <Typography
                        color="textPrimary"
                        sx={{ mb: 1 }}
                        variant="h6"
                      >
                        Initial Care Plan:
                      </Typography>
                      <Typography
                        color="textPrimary"
                        sx={{ mb: 1 }}
                        variant="h6"
                      >
                        Date Finalized:
                      </Typography>
                    </Stack>

                    <Divider />
                    <Typography
                      color="textPrimary"
                      sx={{ mb: 1 }}
                    >
                      Target/ Goals agreed Upon: :
                    </Typography>
                    <Typography
                      color="textPrimary"
                      sx={{ mb: 1 }}
                    >
                      Barriers :
                    </Typography>

                    <Typography
                      color="textPrimary"
                      sx={{ mb: 1 }}
                      variant="h6"
                    >
                      Problems/ Symptoms :
                    </Typography>
                    <Typography
                      color="textPrimary"
                      sx={{ mb: 1 }}
                    >
                      Expected Outcomes :
                    </Typography>
                    <Typography
                      color="textPrimary"
                    >
                      Vitals :
                    </Typography>
                    <Typography
                      color="textPrimary"
                    >
                      Medication :
                    </Typography>
                    <Typography
                      color="textPrimary"
                    >
                      Allergies :
                    </Typography>
                  </Card>
                </Grid>
                {/* 2 */}
                <Grid
                  item
                  md={6}
                  xs={12}
                >
                  <Card
                    sx={{
                      display: 'grid',
                      gap: 3,
                      gridAutoFlow: 'row',
                      p: 3
                    }}
                    variant="outlined"
                  >
                    <Stack spacing={10} direction="row">
                      <Typography
                        color="textPrimary"
                        sx={{ mb: 1 }}
                        variant="h6"
                      >
                        Care Plan Update:
                      </Typography>
                      <Typography
                        color="textPrimary"
                        sx={{ mb: 1 }}
                        variant="h6"
                      >
                        Month:
                      </Typography>
                    </Stack>

                    <Divider />
                    <Typography
                      color="textPrimary"
                      sx={{ mb: 1 }}
                    >
                      Chronic Conditions Update:
                    </Typography>
                    <Typography
                      color="textPrimary"
                      sx={{ mb: 1 }}
                    >
                      Update on progress toward Goals :
                    </Typography>

                    <Typography
                      color="textPrimary"
                      sx={{ mb: 1 }}
                    >
                      Barriers reduced:
                    </Typography>
                    <Typography
                      color="textPrimary"
                      sx={{ mb: 1 }}
                    >
                      improvement in symptons :
                    </Typography>
                    <Typography
                      color="textPrimary"
                    >
                      outcome achieved :
                    </Typography>
                    <Typography
                      color="textPrimary"
                    >
                      Vital Medication :
                    </Typography>
                    <Typography
                      color="textPrimary"
                    >
                      Allergies :
                    </Typography>
                  </Card>
                </Grid>
              </Grid>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <Typography
                color="textPrimary"
                sx={{ mb: 1 }}
                variant="h6"
              >
                Current Month: June
              </Typography>
              <Typography
                color="textPrimary"
                sx={{ mb: 1 }}
                variant="h6"
              >
                Total Time Logged : 5 min
              </Typography>
              <Typography
                color="textPrimary"
                sx={{ mb: 1 }}
                variant="h6"
              >
                1/1/2022 : Called Patient and Speak
              </Typography>
            </TabPanel>
          </CardContent>
        </Card>
      </Grid>
    </Container>
  );
};

export default PatientDashboard;
