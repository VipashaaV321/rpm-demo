import { Link as RouterLink } from 'react-router-dom';
import { Box, Button, Container, Typography } from '@mui/material';

export const ConsentForm = () => (
  <Box sx={{ backgroundColor: 'background.default' }}>
    <Container
      maxWidth="md"
      sx={{
        px: 5,
        py: 14,
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column'
      }}
    >
      <Typography
        align="center"
        color="textPrimary"
        sx={{ my: 2 }}
        variant="h3"
      >
        <u>
          Remote Patient Monitoring (RPM) Consent Form
        </u>
      </Typography>
      <Typography
        align="center"
        color="textSecondary"
        variant="body2"
      >
        I understand that.
      </Typography>
      <Typography
        color="textSecondary"
        variant="body2"
      >
        <ul>
          <li>
            I am the only person who should be using the remote monitoring device(s) as instructed.
            I will not use the device(s) for reasons other than my own personal health monitoring.
            understand that I can only participate in this program with one Medical Provider at a
            time.
          </li>
          <li>
            I will not tamper with the RPM device(s).
            I understand that am responsible for any fees associated with misuse of the device(s).
          </li>
          <li>
            I understand the device(s) are only designed for the RPM program.
            The device(s) is meant to collect vital readings as prescribed by my
            Physician and transfer those readings to an on-line service,
            I understand that RPM is NOT AN EMERGENCY RESPONSE UNIT. I
            understand that must call 911 for immediate medical emergencies.
          </li>
          <li>
            I am aware that my readings will be transmitted from RPM device(s)
            to a software platform in a
            safe and secure manner. I can withdraw my consent to participate in this program,
            and revoke service at any time by returning the device(s).
          </li>
          <li>
            I will do my best to take my readings every day.
            am aware that a Remote Patient Monitoring Qualified Health Professional
            will view my readings.
            I will be contacted, by phone, or SMS to remind me to take my readings,
            review and discuss my results and progress.
          </li>

        </ul>

      </Typography>
      <Typography
        color="textSecondary"
        variant="body2"
        sx={{ m: 3 }}
      >
        I have read and understood the information (Print your name) and consent to participate
        in the Remote Patient Monitoring program as stated above.
        I am aware that this consent is valid as long as I am in possession of the RPM device(s).
      </Typography>
      <Typography
        color="textSecondary"
        variant="body2"
        sx={{ m: 3 }}
      >
        Date: ________________________________ (DD/MM/YY)
      </Typography>
      <Typography
        color="textSecondary"
        variant="body2"
        sx={{ m: 3 }}
      >
        Signature of Patient or Authorized Person
        <br />
        <br />
        ____________________________________________
      </Typography>
      <Button
        color="primary"
        component={RouterLink}
        sx={{ mt: 2 }}
        to="/device-manager"
        variant="contained"
      >
        Begin
      </Button>
    </Container>
  </Box>
);
