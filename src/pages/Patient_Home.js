import { Card, Container, Grid, Typography, CardHeader, Divider, Stack, Button, CardContent, TableContainer, Table } from '@mui/material';
import LineChart from 'react-linechart';
import '../../node_modules/react-linechart/dist/styles.css';

const data = [
  {
    color: 'steelblue',
    points: [{ x: 1, y: 2 }, { x: 3, y: 5 }, { x: 7, y: 3 }, { x: 8, y: 4 }]
  }
];

export const PatientHome = () => (
  <>
    <Container fixed sx={{ p: 3 }}>
      <Grid
        item
        xs={12}
      >
        <Card
          variant="outlined"
        >
          <CardHeader title="Patient Home" sx={{ background: '#F0F8FF' }} />
          <TableContainer>
            <Stack sx={{ p: 2 }}>
              Patient Name :
              <br />
              Address :_____________
              DOB :_________________
              Phone :_______________
              Email :_______________
              Communication Preference:____________________
              <br />
              Patient Portal: Yes / No
              <br />
              PCP :________________________
              Care Team :_____________________
              <br />
              Care Plan :
              <Stack spacing={2} direction="row" sx={{ mt: 2 }}>
                <Button href="/care-team" variant="contained"> ( Create New )</Button>
                <Button variant="contained">Edit</Button>
              </Stack>
            </Stack>
          </TableContainer>
          <Divider />
          <CardContent>
            <Divider />
            <TableContainer>
              <Stack spacing={2} direction="row" sx={{ m: 1 }}>
                <Button variant="outlined">Refresh</Button>
                <Button variant="outlined" href="/device-manager">RPM Setup</Button>
                <Button variant="outlined" href="/care-coordination">Care Team</Button>
                <Button variant="outlined">Report</Button>
              </Stack>
            </TableContainer>
            <Divider />
            <Grid
              container
              spacing={3}
              sx={{
                p: 4
              }}
            >
              <Grid
                item
                md={4}
                xs={12}
              >
                <Card
                  sx={{
                    display: 'grid',
                    gap: 3,
                    gridAutoFlow: 'row',
                    p: 2,
                    height: 200
                  }}
                  variant="outlined"
                >

                  <Grid container>
                    <Grid xs={8} alignItems="left">
                      Remote Monitoring
                    </Grid>
                    <Grid xs={4} alignItems="right">
                      View all / Edit
                    </Grid>
                    Last 10 Reading
                  </Grid>
                </Card>
              </Grid>
              <Grid
                item
                md={4}
                xs={12}
              >
                <Card
                  sx={{
                    display: 'grid',
                    gap: 3,
                    gridAutoFlow: 'row',
                    p: 3,
                    height: 200
                  }}
                  variant="outlined"
                >
                  <Grid container rowSpacing={1}>
                    <Grid xs={8} alignItems="left">
                      Remote Monitoring- Abnormal
                    </Grid>
                    <Grid xs={4} alignItems="right">
                      View all / Edit
                    </Grid>
                    Last 3 Reading
                  </Grid>
                </Card>
              </Grid>
              <Grid
                item
                md={4}
                xs={12}
              >
                <Card
                  sx={{
                    display: 'grid',
                    gap: 3,
                    gridAutoFlow: 'row',
                    p: 3,
                    height: 200
                  }}
                  variant="outlined"
                >
                  <Grid container rowSpacing={1}>
                    <Grid xs={8} alignItems="left">
                      FollowUp / Add New
                    </Grid>
                    <Grid xs={4} alignItems="right">
                      View all / Edit
                    </Grid>
                  </Grid>

                </Card>
              </Grid>
            </Grid>
            {/* 2ndrow */}
            <Grid
              container
              spacing={3}
              sx={{
                p: 4,
              }}

            >
              <Grid
                item
                md={4}
                xs={12}
              >
                <Card
                  sx={{
                    display: 'grid',
                    gap: 3,
                    gridAutoFlow: 'row',
                    p: 3,
                    height: 300
                  }}
                  variant="outlined"
                >
                  <Typography
                    color="textPrimary"
                  >
                    Billing Log
                  </Typography>
                  <TableContainer
                    sx={{
                      p: 2
                    }}
                    style={{
                      border: '1px solid #ccc',
                    }}
                  >
                    <Table>
                      <tr style={{
                        border: '1px solid #ccc',
                      }}
                      >
                        <th>Billing Cycle</th>
                        <th>Upload Days</th>
                        <th>Billing status</th>
                        <th>Action</th>
                      </tr>
                      <tr>
                        <td>1/2/22 to 2/2/22</td>
                        <td>19</td>
                        <td>Pending</td>
                        <td>{' '}</td>
                      </tr>
                      <tr>
                        <td>{' '}</td>
                        <td>{' '}</td>
                        <td>{' '}</td>
                        <td>{' '}</td>
                      </tr>
                    </Table>
                    <hr />
                  </TableContainer>
                </Card>
              </Grid>
              <Grid
                item
                md={4}
                xs={12}
              >
                <Card
                  sx={{
                    display: 'grid',
                    gap: 3,
                    gridAutoFlow: 'row',
                    p: 3,
                    height: 300
                  }}
                  variant="outlined"
                >
                  <Typography
                    color="textPrimary"
                  >
                    Care Coordination Time Sheet
                  </Typography>
                  <Divider />
                  <Typography
                    color="textPrimary"
                  >
                    Current Month : ____________
                  </Typography>
                  <Typography
                    color="textPrimary"
                  >
                    Min Completed : 10
                  </Typography>
                  <Typography
                    color="textPrimary"
                  >
                    Last Coordination
                  </Typography>
                </Card>
              </Grid>
              <Grid
                item
                md={4}
                xs={12}
              >
                <Card
                  sx={{
                    display: 'grid',
                    gap: 3,
                    gridAutoFlow: 'row',
                    p: 2,
                    height: 300
                  }}
                  variant="outlined"
                >
                  <Typography
                    color="textPrimary"
                  >
                    Remote Monitoring-Trend
                    <br />
                    Device :_________
                  </Typography>
                  <Divider />
                  <LineChart
                    width={200}
                    height={200}
                    data={data}
                    xLabel=" "
                    yLabel=" "
                  />

                </Card>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>

    </Container>

  </>
);
