import React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import FormControlLabel from '@mui/material/FormControlLabel';
import { Grid, Card, CardHeader, Divider, CardContent, Container, Radio, Stack, Button, RadioGroup, FormControl, FormLabel } from '@mui/material';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const PatientSetup = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <Container fixed sx={{ p: 5 }}>
      <Grid
        item
        xs={12}
      >
        <Grid
          item
          xs={12}
        >
          <Card
            variant="outlined"

          >
            <CardHeader title="Patient Setup" sx={{ background: '#F0F8FF' }} />
            <Divider />
            <CardContent>
              <Box sx={{ width: '100%' }}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                  <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    <Tab label="Elligible Crietria" {...a11yProps(0)} />
                    <Tab label="Alert Customization" {...a11yProps(1)} />
                  </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                  <Box sx={{ flexGrow: 1 }}>
                    <Grid
                      container
                      spacing={3}
                      direction="row"
                      justifyContent="center"
                    >
                      <Grid
                        item
                        xs={12}
                        md={4}
                        justifyContent="center"
                        alignItems="center"
                      >
                        <Card variant="outlined" sx={{ p: 3 }}>
                          <FormControl>
                            <FormLabel id="demo-radio-buttons-group-label">
                              Chronic Conditions:
                            </FormLabel>
                            <RadioGroup
                              aria-labelledby="demo-radio-buttons-group-label"
                              defaultValue=""
                              name="radio-buttons-group"
                            >
                              <FormControlLabel value="female" control={<Radio />} label="Anxiety" />
                              <FormControlLabel value="male" control={<Radio />} label="Acquired Hypothyroidism" />
                              <FormControlLabel value="other" control={<Radio />} label="Chronic Kidney Disease" />
                              <FormControlLabel value="female" control={<Radio />} label="Anxiety" />
                              <FormControlLabel value="male" control={<Radio />} label="Depression" />
                              <FormControlLabel value="other" control={<Radio />} label="Hypertension" />
                              <FormControlLabel value="female" control={<Radio />} label="Hyperlipidemia" />
                              <FormControlLabel value="male" control={<Radio />} label="Ischemic Heart Disease" />
                              <FormControlLabel value="other" control={<Radio />} label="Diabetes" />
                              <FormControlLabel value="female" control={<Radio />} label="HIV" />
                              <FormControlLabel value="male" control={<Radio />} label="Cancer" />
                              <FormControlLabel value="other" control={<Radio />} label="A-Fib" />
                              <FormControlLabel value="other" control={<Radio />} label="Congestive Heart Failure" />
                              <FormControlLabel value="other" control={<Radio />} label="Obesity" />
                              <FormControlLabel value="other" control={<Radio />} label="COPD" />
                              <FormControlLabel value="other" control={<Radio />} label="Asthma" />
                            </RadioGroup>
                          </FormControl>
                        </Card>
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        md={4}
                        justifyContent="center"
                        alignItems="center"
                      >
                        <Card variant="outlined" sx={{ p: 3 }}>
                          <FormControl>
                            <FormLabel id="demo-radio-buttons-group-label">Vital Range</FormLabel>
                            <RadioGroup
                              aria-labelledby="demo-radio-buttons-group-label"
                              defaultValue=""
                              name="radio-buttons-group"
                            >
                              <FormControlLabel value="female" control={<Radio />} label="BP Diastolic > 90" />
                              <FormControlLabel value="male" control={<Radio />} label="BP Diastolic > 100" />
                              <FormControlLabel value="other" control={<Radio />} label="BP Systolic > 140" />
                              <FormControlLabel value="female" control={<Radio />} label="BP Systolic > 150" />
                              <FormControlLabel value="male" control={<Radio />} label="BMI > 25" />
                              <FormControlLabel value="other" control={<Radio />} label="BMI > 30" />
                              <FormControlLabel value="female" control={<Radio />} label="HbA1C > 6" />
                              <FormControlLabel value="male" control={<Radio />} label="HbA1C > 7" />
                            </RadioGroup>
                          </FormControl>
                        </Card>
                      </Grid>
                      <Grid
                        item
                        md={4}
                        xs={12}
                        justifyContent="center"
                        alignItems="center"
                      >
                        <Card variant="outlined" sx={{ p: 3 }}>
                          <FormControl>
                            <FormLabel id="demo-radio-buttons-group-label">
                              Payor Selection:
                            </FormLabel>
                            <RadioGroup
                              aria-labelledby="demo-radio-buttons-group-label"
                              defaultValue=""
                              name="radio-buttons-group"
                            >
                              <FormControlLabel value="female" control={<Radio />} label="Highmark Medicare" />
                              <FormControlLabel value="male" control={<Radio />} label="Aetna" />
                              <FormControlLabel value="other" control={<Radio />} label="Horizon BCBS" />
                            </RadioGroup>
                          </FormControl>
                        </Card>
                      </Grid>
                      <Stack spacing={2} direction="row" sx={{ mt: 3 }}>
                        <Button variant="contained" href="/remote-patient-monitoring-list">Save and Regenerate</Button>
                        <Button variant="contained">Cancel</Button>
                      </Stack>
                    </Grid>
                  </Box>
                </TabPanel>
                <TabPanel value={value} index={1}>
                  Alerts
                </TabPanel>
              </Box>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};

export default PatientSetup;
