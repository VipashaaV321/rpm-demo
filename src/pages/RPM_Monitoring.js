import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Checkbox from '@mui/material/Checkbox';

const RPMMonitoring = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="RPM Monitoring" sx={{ background: '#F0F8FF' }} />
        <Divider />
        <CardContent>
          <TableContainer>
            <Stack spacing={2} direction="row" sx={{ m: 2 }}>
              <Button variant="outlined" href="/rpm-monitoring">Refresh</Button>
              <Button variant="outlined">Print List</Button>
              <Button variant="outlined">Export to Excel</Button>
              <Button variant="outlined" href="/dashboard">Close</Button>
            </Stack>
          </TableContainer>
          <Divider />
          <FormControl sx={{ m: 1 }}>
            <RadioGroup
              row
              aria-labelledby="demo-row-radio-buttons-group-label"
              name="row-radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="All Patient" />
              <FormControlLabel value="male" control={<Radio />} label="Patient" />
            </RadioGroup>
          </FormControl>
          <br />
          <FormControl sx={{ m: 1 }}>
            <RadioGroup
              row
              aria-labelledby="demo-row-radio-buttons-group-label"
              name="row-radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio />} label="Today's" />
              <FormControlLabel value="male" control={<Radio />} label="Last 7 day's" />
              <FormControlLabel value="other" control={<Radio />} label="30 day's" />
            </RadioGroup>
            <p>   from ____________________ to _________________________</p>
          </FormControl>
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardContent>
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>
                        <Checkbox />
                      </TableCell>
                      <TableCell align="right">Patient Name</TableCell>
                      <TableCell align="right">Upload Date</TableCell>
                      <TableCell align="right">Device</TableCell>
                      <TableCell align="right">Data</TableCell>
                      <TableCell align="center">Threshold</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell>
                        <Checkbox />
                      </TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Checkbox />
                      </TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                      <TableCell align="right">{' '}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
                <Divider />
              </TableContainer>
            </CardContent>
          </Card>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default RPMMonitoring;
