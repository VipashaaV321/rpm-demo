import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, Typography, Button } from '@mui/material';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';

const PatientCommunication = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
      >
        <CardHeader title="Patient Communication" sx={{ background: '#F0F8FF' }} />
        <Divider />
        <CardContent>
          <Stack spacing={2} sx={{ m: 2 }}>
            <Typography mt={2}>Patient Name</Typography>
            <Typography mt={2}>Phone Number : Prefilled</Typography>
            <Typography mt={2}>Email : Prefilled</Typography>

          </Stack>
          <Button variant="outlined" href="/patient-registration" sx={{ m: 2 }}>Edit</Button>
          <Card
            variant="outlined"
            sx={{
              mt: 3,
            }}
          >
            <CardContent>
              <Typography mt={2}>Enter Message :</Typography>
              <FormControl fullWidth variant="standard">
                <TextField
                  label="Recipient"
                  id="outlined-start-adornment"
                  fullWidth
                  sx={{
                    mt: 2
                  }}
                  InputProps={{
                    endAdornment: <InputAdornment position="start">+</InputAdornment>,
                  }}
                />
              </FormControl>
              <TextField
                id="outlined-multiline-static"
                label="Enter Message Here"
                multiline
                fullWidth
                rows={5}
                sx={{
                  mt: 2
                }}
              />
              <Button variant="contained" href="/patient-home" sx={{ mt: 1 }}>Send Message</Button>
            </CardContent>
          </Card>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default PatientCommunication;
