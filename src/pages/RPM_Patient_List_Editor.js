import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, Box } from '@mui/material';
import Container from '@mui/material/Container';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';

const RemotePatientListEditor = () => (
  <Container fixed sx={{ p: 5 }}>
    <Grid
      item
      xs={12}
    >
      <Card
        variant="outlined"
        sx={{
          mt: 3,
        }}
      >
        <CardHeader title="RPM Patient List Editor: " sx={{ background: '#F0F8FF' }} />
        <Divider />
        <CardContent>
          <Box sx={{ flexGrow: 1 }}>
            <Grid
              container
              spacing={3}
              direction="row"
              justifyContent="center"
            >
              <Grid
                item
                xs={12}
                md={4}
                justifyContent="center"
                alignItems="center"
              >
                <Card variant="outlined" sx={{ p: 3 }}>
                  <FormControl>
                    <FormLabel id="demo-radio-buttons-group-label">
                      Chronic Conditions:
                    </FormLabel>
                    <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue=""
                      name="radio-buttons-group"
                    >
                      <FormControlLabel value="female" control={<Radio />} label="Anxiety" />
                      <FormControlLabel value="male" control={<Radio />} label="Acquired Hypothyroidism" />
                      <FormControlLabel value="other" control={<Radio />} label="Chronic Kidney Disease" />
                      <FormControlLabel value="female" control={<Radio />} label="Anxiety" />
                      <FormControlLabel value="male" control={<Radio />} label="Depression" />
                      <FormControlLabel value="other" control={<Radio />} label="Hypertension" />
                      <FormControlLabel value="female" control={<Radio />} label="Hyperlipidemia" />
                      <FormControlLabel value="male" control={<Radio />} label="Ischemic Heart Disease" />
                      <FormControlLabel value="other" control={<Radio />} label="Diabetes" />
                      <FormControlLabel value="female" control={<Radio />} label="HIV" />
                      <FormControlLabel value="male" control={<Radio />} label="Cancer" />
                      <FormControlLabel value="other" control={<Radio />} label="A-Fib" />
                      <FormControlLabel value="other" control={<Radio />} label="Congestive Heart Failure" />
                      <FormControlLabel value="other" control={<Radio />} label="Obesity" />
                      <FormControlLabel value="other" control={<Radio />} label="COPD" />
                      <FormControlLabel value="other" control={<Radio />} label="Asthma" />
                    </RadioGroup>
                  </FormControl>
                </Card>
              </Grid>
              <Grid
                item
                xs={12}
                md={4}
                justifyContent="center"
                alignItems="center"
              >
                <Card variant="outlined" sx={{ p: 3 }}>
                  <FormControl>
                    <FormLabel id="demo-radio-buttons-group-label">Vital Range</FormLabel>
                    <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue=""
                      name="radio-buttons-group"
                    >
                      <FormControlLabel value="female" control={<Radio />} label="BP Diastolic > 90" />
                      <FormControlLabel value="male" control={<Radio />} label="BP Diastolic > 100" />
                      <FormControlLabel value="other" control={<Radio />} label="BP Systolic > 140" />
                      <FormControlLabel value="female" control={<Radio />} label="BP Systolic > 150" />
                      <FormControlLabel value="male" control={<Radio />} label="BMI > 25" />
                      <FormControlLabel value="other" control={<Radio />} label="BMI > 30" />
                      <FormControlLabel value="female" control={<Radio />} label="HbA1C > 6" />
                      <FormControlLabel value="male" control={<Radio />} label="HbA1C > 7" />
                    </RadioGroup>
                  </FormControl>
                </Card>
              </Grid>
              <Grid
                item
                xs={12}
                md={4}
                justifyContent="center"
                alignItems="center"
              >
                <Card variant="outlined" sx={{ p: 3 }}>
                  <FormControl>
                    <FormLabel id="demo-radio-buttons-group-label">
                      Payor Selection:
                    </FormLabel>
                    <RadioGroup
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue=""
                      name="radio-buttons-group"
                    >
                      <FormControlLabel value="female" control={<Radio />} label="Highmark Medicare" />
                      <FormControlLabel value="male" control={<Radio />} label="Aetna" />
                      <FormControlLabel value="other" control={<Radio />} label="Horizon BCBS" />
                    </RadioGroup>
                  </FormControl>
                </Card>
              </Grid>
              <Stack spacing={2} direction="row" sx={{ mt: 3 }}>
                <Button variant="contained" href="/remote-patient-monitoring-list">Save and Regenerate</Button>
                <Button variant="contained" href="/dashboard">Cancel</Button>
              </Stack>
            </Grid>
          </Box>
        </CardContent>
      </Card>
    </Grid>
  </Container>
);

export default RemotePatientListEditor;
