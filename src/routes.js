import { Navigate } from 'react-router-dom';
import { Layout } from './components/layout';
import { Icons } from './pages/icons';
import { NotFound } from './pages/not-found';
import { Orders } from './pages/orders';
import { MainDashboard } from './pages/Main-Dashboard';
import { Settings } from './pages/settings';
import { Theme } from './pages/theme';
import { ConsentForm } from './pages/Consent_Form';
import { RemotePatientMonitoringList } from './pages/Remote_Patient_Monitoring_List';
import { PatientEnroll } from './pages/Patient_Enroll';
import { NewDevice } from './pages/New_Device';
import DeviceManager from './pages/Device_Manager';
import AbnormalReading from './pages/Abonormal_Reading';
import RPMCompliance from './pages/RPM_Compliance';
import PracticeSetup from './pages/Practice_Setup';
import PatientSetup from './pages/Patient_setup';
import RPMMonitoring from './pages/RPM_Monitoring';
import AlertPage from './pages/Alert';
import PatientCommunication from './pages/Patient_Communication';
import RemotePatientListEditor from './pages/RPM_Patient_List_Editor';
import NewFollowup from './pages/New_Followup';
import CareCoordination from './pages/Care_Coordination';
import ActiveUsers from './pages/Active_Users';
import BillingReport from './pages/RPM_Billing_Report';
import CareTeam from './pages/Care_Team';
import DeviceManagement from './pages/Device-management';
import Followup from './pages/Follow_Up';
import { PatientHome } from './pages/Patient_Home';
import PatientRegistration from './pages/Patient_Registration';
import PatientDashboard from './pages/patient-dashboard';

export const routes = [
  {
    path: '/',
    element: <Navigate to="/dashboard" />
  },
  {
    path: 'dashboard',
    element: <Layout />,
    children: [
      {
        path: '',
        element: <MainDashboard />
      },
      {
        path: 'orders',
        element: <Orders />
      },
      {
        path: 'settings',
        element: <Settings />
      },
      {
        path: 'theme',
        element: <Theme />
      },
      {
        path: 'icons',
        element: <Icons />
      },
      {
        path: '*',
        element: <Navigate to="/404" />
      },
      {
        path: 'consent-form',
        element: <ConsentForm />
      },
      {
        path: 'remote-patient-monitoring-list',
        element: <RemotePatientMonitoringList />
      },
      {
        path: 'patient-enroll',
        element: <PatientEnroll />
      },
      {
        path: 'new-device',
        element: <NewDevice />
      },
      {
        path: 'device-manager',
        element: <DeviceManager />
      },
      {
        path: 'abnormal-reading',
        element: <AbnormalReading />
      },
      {
        path: 'rpm-compliance',
        element: <RPMCompliance />
      },
      {
        path: 'practice-setup',
        element: <PracticeSetup />
      },
      {
        path: 'patient-setup',
        element: <PatientSetup />
      },
      {
        path: 'rpm-monitoring',
        element: <RPMMonitoring />
      },
      {
        path: 'alert',
        element: <AlertPage />
      },
      {
        path: 'patient-communication',
        element: <PatientCommunication />
      },
      {
        path: 'remote-patient-list-editor',
        element: <RemotePatientListEditor />
      },
      {
        path: 'new-followup',
        element: <NewFollowup />
      },
      {
        path: 'care-coordination',
        element: <CareCoordination />
      },
      {
        path: 'active-users',
        element: <ActiveUsers />
      },
      {
        path: 'rpm-billing-report',
        element: <BillingReport />
      },
      {
        path: 'care-team',
        element: <CareTeam />
      },
      {
        path: 'device-management',
        element: <DeviceManagement />
      },
      {
        path: 'follow-up',
        element: <Followup />
      },
      {
        path: 'patient-home',
        element: <PatientHome />
      },
      {
        path: 'patient-registration',
        element: <PatientRegistration />
      },
      {
        path: 'patient-dashboard',
        element: <PatientDashboard />
      }
    ]
  },
  {
    path: 'consent-form',
    element: <ConsentForm />
  },
  {
    path: 'remote-patient-monitoring-list',
    element: <RemotePatientMonitoringList />
  },
  {
    path: 'patient-enroll',
    element: <PatientEnroll />
  },
  {
    path: 'new-device',
    element: <NewDevice />
  },
  {
    path: 'device-manager',
    element: <DeviceManager />
  },
  {
    path: 'abnormal-reading',
    element: <AbnormalReading />
  },
  {
    path: 'rpm-compliance',
    element: <RPMCompliance />
  },
  {
    path: 'practice-setup',
    element: <PracticeSetup />
  },
  {
    path: 'patient-setup',
    element: <PatientSetup />
  },
  {
    path: 'rpm-monitoring',
    element: <RPMMonitoring />
  },
  {
    path: 'alert',
    element: <AlertPage />
  },
  {
    path: 'patient-communication',
    element: <PatientCommunication />
  },
  {
    path: 'remote-patient-list-editor',
    element: <RemotePatientListEditor />
  },
  {
    path: 'new-followup',
    element: <NewFollowup />
  },
  {
    path: 'care-coordination',
    element: <CareCoordination />
  },
  {
    path: 'active-users',
    element: <ActiveUsers />
  },
  {
    path: 'rpm-billing-report',
    element: <BillingReport />
  },
  {
    path: 'care-team',
    element: <CareTeam />
  },
  {
    path: 'device-management',
    element: <DeviceManagement />
  },
  {
    path: 'follow-up',
    element: <Followup />
  },
  {
    path: 'patient-home',
    element: <PatientHome />
  },
  {
    path: 'patient-registration',
    element: <PatientRegistration />
  },
  {
    path: 'patient-dashboard',
    element: <PatientDashboard />
  },
  {
    path: '404',
    element: <NotFound />
  }
];
